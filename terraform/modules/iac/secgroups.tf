resource "aws_security_group" "graylog_server" {
  name        = "GrayLog Security Group"

  dynamic "ingress" {
    for_each = "${var.graylog_ports_tcp}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    for_each = "${var.graylog_ports_udp}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "GrayLog Server Security Group"
  }
}


resource "aws_security_group" "test_server" {
  name        = "Test Security Group"

  dynamic "ingress" {
    for_each = "${var.test_ports_tcp}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    for_each = "${var.test_ports_udp}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Test Server Security Group"
  }
}
