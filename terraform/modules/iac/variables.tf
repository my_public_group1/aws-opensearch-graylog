variable "route53_hosted_zone_name" {
  type = string
}

variable "ssh_key_private" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "allowed_cidr_blocks" {
  type = list
  default = ["0.0.0.0/0"]
}

variable "aws_keyname" {
  type = string
}

variable "graylog_ports_udp" {
  type = list
  default = []
}

variable "graylog_ports_tcp" {
  type = list
  default = ["9000", "9200", "9300", "22", "27017", "5001", "5002", "5003", "80"]
}

variable "test_ports_udp" {
  type = list
  default = []
}

variable "test_ports_tcp" {
  type = list
  default = ["22"]
}

variable "os_user" {
  type = string
}

variable "os_pass" {
  type = string
}

variable "graylog_secret" {
  type = string
}

variable "graylog_sha2" {
  type = string
}
