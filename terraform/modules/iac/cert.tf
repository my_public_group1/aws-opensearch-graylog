module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 3.0"

  zone_id = "${data.aws_route53_zone.zone.zone_id}"

  domain_name               = "${var.route53_hosted_zone_name}"
  subject_alternative_names = ["*.${var.route53_hosted_zone_name}"]

  wait_for_validation = true
}
