# Запускаем GrayLog сервер
resource "aws_instance" "graylog_server" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.xlarge"
  vpc_security_group_ids = [aws_security_group.graylog_server.id]
  key_name = "${var.aws_keyname}"

  root_block_device {
    volume_size = 30
  }

  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "GrayLog Server"
  }

  depends_on = [
    aws_opensearch_domain.skill_study
  ]

  provisioner "remote-exec" {
    inline = ["sudo apt-get update -y",
              "sudo apt-get install python3 -y"]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      echo '${self.public_ip}' > ../ansible/graylog.txt
      ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_key_private} ../ansible/graylog/main.yml \
        --extra-vars 'input_domain=${var.route53_hosted_zone_name} os_user=${var.os_user} os_pass=${var.os_pass} graylog_secret=${var.graylog_secret} graylog_sha2=${var.graylog_sha2}' \
        -b -v
    EOT
  }
}

# Запускаем Test сервер
resource "aws_instance" "test_server" {
  # с выбранным образом
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.test_server.id]
  key_name = "${var.aws_keyname}"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Test Server"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt-get update -y",
              "sudo apt-get install python3 -y"]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      echo 'done'
    EOT
  }
}

