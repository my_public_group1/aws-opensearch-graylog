resource "aws_route53_record" "opensearch" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "os.${var.route53_hosted_zone_name}"
  type    = "CNAME"
  ttl     = "5"
  records = ["${aws_opensearch_domain.skill_study.endpoint}"]
  depends_on = [aws_opensearch_domain.skill_study]
}

resource "aws_route53_record" "graylog" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "graylog.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.graylog_server.public_ip}"]
}
